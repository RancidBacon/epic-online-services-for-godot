## Epic Online Services support for Godot

*This could be the future home of the "Epic Online Services for Godot" addon.*

(Not associated with Epic Games or Godot.)


![](endov-eos-unlocked-menu-small.png)

Download "The Endov Society" the interactive video game software
provided for entertainment purposes (and definitely *not* a mere "tech
demo") built with this addon:

 * <https://rancidbacon.itch.io/the-endov-society>


Learn more about Epic Online Services: <https://dev.epicgames.com/docs/services/en-US/>

(See below for [current project status](#current-project-status-aka-wheres-the-code).)


### Screenshots Update (New! 2021)

In 2020 & 2021 I used this Godot addon to...add on...multiplayer
support to two game jam entries:

![](assets/procjam-2020-mutango-multiplayer-screencap.gif)  
_Description: Screencap shows the windows of two players in a three player
lobby. When the first player closes their window they can be seen
leaving the lobby in the window of the second player who remains
connected._

![](assets/eos-multiplayer-movement--screencap.gif)  
_Description: Screencap shows windows of two players in a procedurally generated dungeon. The player in the right window moves around & their updated position can be seen in the second player's window._

In both cases the multiplayer support used only the EOS "lobby"
functionality to broadcast player state change rather than EOS P2P
support which has not yet been wrapped. While not exactly optimal,
using the lobby feature in this way is certainly enough to serve
as a proof of concept for a game jam entry.

See below for more details, screenshots & how to try out the
dungeon.

#### 7 Day Roguelike Jam Entry: The Dungeon Dies With Us (2021)

![](assets/7drl-jam--tddwu--online-multiplayer--screenshot.png)

#### ProcJam Entry: Mutango! (2020)

![](assets/procjam-2020-mutango-four-player-screenshot.png)

![](assets/procjam-2020-mutango-three-player-screenshot.png)


### Current project status (a.k.a "Where's the code?")

While it has been my intention from the start of development on this
project to release it under an Open Source license, the "Epic Online
Services for Godot" addon does not yet have a source release/download
available.

This is for two primary reasons:

 * Economic

   Current economic realities mean I am not in a position to support
   development of this project with solely my own resources.

   To get the project to the point where it supports even the feature
   sub-set used in "The Endov Society" has taken a significant
   investment of time & effort. Significant effort is still needed to
   support the rest of the EOS feature set; provide an idiomatic Godot
   interface; create Godot-specific examples of integrating EOS
   features and document their use.

   Development started when EOS was released in 2019 and reached basic
   proof-of-concept level then resumed in May 2020 on a side-project
   basis when additional features were released in an EOS update.

   In August (after investing significant time & effort in this
   project; in addition to the "WASM Engine for Godot" WebAssembly
   runtime add-on; and, extending Foreigner) I made the decision to
   release "The Endov Society" game as an entertaining and interactive
   way to... show... what was possible with the addon and gauge the
   level of financial support to fund further development & release.

   Not really a fan of this approach but I still gotta eat and keep a
   roof over my head.

   So, here we are. :)

 * Legal

   Due to the EOS SDK License agreement and the method by which
   Foreign Function Interface bindings work (particularly for
   non-compiled languages) it is unclear whether Epic Games would
   permit distribution of part or all of the "Epic Online Services for
   Godot" addon under an Open Source license--nor how litigious they
   might be about it. ;)

If you or your company would like to financially support the
development & release of the "Epic Online Services for Godot" addon
under an Open Source license please get in touch via Twitter
(https://twitter.com/rancidbacon) or create an issue in this
repository (<https://gitlab.com/RancidBacon/epic-online-services-for-godot/-/issues>).


#### Look a gift horse in the mouth

At this point I would also like to note that while I have written this
add-on, it should not be taken to mean I necessarily endorse Epic
Online Services as a service.

While EOS is provided at no direct financial cost to developers it is
important to keep in mind that Epic Games is a company with investors
who expect a financial return and EOS is a proprietary service with a
closed source SDK.

I encourage you to evaluate whether the given set of trade-offs from
use of EOS is suitable for you, your game and your game's community of
players--now and in the future.


### Current code status

The "Epic Online Services for Godot" addon uses
[Foreigner](https://github.com/and3rson/foreigner) (a
[`libffi`](https://sourceware.org/libffi/) wrapper for Godot) which
enables the majority of Godot-specific code to be written in
GDScript. This means that changes don't require per-platform
compilation and knowledge of C/C++ isn't necessary for adding
functionality.

A set of tools are used to--semi-automatically, currently--create a
set of bindings to the functions, enums & structs etc exposed by the
Epic Online Services shared library:

 * The first tool parses the macros used to define the EOS SDK API in
   the `.h` header files and outputs a JSON file with information
   about each item.

 * The second tool reads the JSON file and generates a GDScript file
   which is used to call functions in the shared library via
   Foreigner.

*Part of the reason for this two-part process is to potentially make
it easier for other Open Source projects to generate bindings for
other languages.* (This could also be used to assist in adding
non-desktop platform support in Godot.)


#### Supported platforms

 * Linux

 * Mac (limited to 10.13+ due to lack of earlier support in the EOS
        SDK shared library; untested with Catalina+)

 * Windows 64-bit


#### Supported services

The Epic Online Services functionality that can currently be used from
GDScript:

 * SDK (v1.6.0)

    * Initialisation/Shutdown/Tick

    * Get version.

    * String conversions.

    * Logging via callback & log level.

    * Per-service non-polling notification support. (Includes caching
      of short-lived strings.)

 * Connect

   * Device ID creation (i.e. no account required to use)

   * Display name.

   * Connect/Login via Device ID

   * Automatic authorization refresh (required every hour)

 * Lobbies

   * Create

   * Search by Lobby Id & Player Id & parameters

   * Add/update Lobby & Member attributes

   * Host migration (supports member being promoted when owner leaves)

   * Not yet: Kick

 * Stats

   * Ingestion

 * Achievements

   * Respond to unlock notification (via stats ingestion)

   * List available

   * List per-player progress

 * Leaderboard

   * List available

   * Not yet: Retrieve

While other services in the SDK have bindings generated for almost all
associated functions they have not yet all been exposed/tested.

Most notably, none of the functionality that requires Epic Account
Services is yet supported.

Caveats:

 * Requires struct/buffer support not yet integrated into upstream
   Foreigner.

 * Requires a per-platform helper library for notification/callback
   support. (Ideally this functionality will be added to Foreigner
   directly.)

 * Allocated memory is generally not yet automatically released.

----

<https://gitlab.com/RancidBacon/epic-online-services-for-godot>
